import { PrismaClient } from "@prisma/client";
const db = new PrismaClient();

async function seed() {
    await Promise.all(
        getMhsw().map(mhsw => {
            return db.mhsw.create({ data: mhsw });
        })
    );
}

seed();

function getMhsw() {
    // shout-out to https://icanhazdadjoke.com/

    return [
        {
            name: "Road worker 01",
            nim: "01",
            batch: "2021"
        },
        {
            name: "Road worker 02",
            nim: "02",
            batch: "2021"
        },
        {
            name: "Road worker 03",
            nim: "03",
            batch: "2021"
        }
    ];
}