import { Form, useActionData } from "remix";
import { ValidateRandomInput } from "~/config/validation";


export default function RandomMhswRoute() {
  var data = useActionData()
  return (
    <div className="card card-grey">
      <Form method="get">
        <div>
          <label>
            Masukkan angka:
          </label>
          <input type="text" name="jumlah"></input>
          <button type="submit">
            Set
          </button>
        </div>
      </Form>
    </div>
  );
}