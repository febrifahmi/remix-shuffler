import type { Mhsw } from "@prisma/client";
import { ActionFunction, LoaderFunction } from "remix";
import {
  Link,
  useLoaderData,
  useCatch,
  redirect,
  useParams
} from "remix";
import { db } from "~/utils/db.server";

type LoaderData = { mhsw: Mhsw };

export const loader: LoaderFunction = async ({
  params
}) => {
  const mhsw = await db.mhsw.findUnique({
    where: { id: params.id }
  });
  if (!mhsw) {
    throw new Response("Not found.", {
      status: 404
    });
  }
  const data: LoaderData = { mhsw };
  return data;
};

export const action: ActionFunction = async ({
  request,
  params
}) => {
  const form = await request.formData();
  if (form.get("_method") === "delete") {
    const mhsw = await db.mhsw.findUnique({
      where: { id: params.id }
    });
    if (!mhsw) {
      throw new Response(
        "Can't delete what does not exist",
        { status: 404 }
      );
    }

    await db.mhsw.delete({ where: { id: params.id } });
    return redirect("/");
  }
};

export default function DelMhswRoute() {
  const data = useLoaderData<LoaderData>();

  return (
    <div className="container card int-atas-bwh gap-to-top-2 d-flex justify-content-center">
      <div className="d-flex flex-column justify-content-center">
        <p className="d-flex">Hapus data mahasiswa:</p>
        <p className="d-flex">{data.mhsw.name} ?</p>
      </div>

      <form method="post">
        <input
          type="hidden"
          name="_method"
          value="delete"
        />
        <button type="submit" className="button">
          Delete
        </button>
      </form>
    </div>
  );
}

export function CatchBoundary() {
  const caught = useCatch();
  const params = useParams();
  switch (caught.status) {
    case 404: {
      return (
        <div className="error-container">
          Huh? What the heck is {params.id}?
        </div>
      );
    }
    case 401: {
      return (
        <div className="error-container">
          Sorry, but {params.id} is not your joke.
        </div>
      );
    }
    default: {
      throw new Error(`Unhandled error: ${caught.status}`);
    }
  }
}

export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);
  const { id } = useParams();
  return (
    <div className="error-container">{`There was an error loading mhsw by the id ${id}. Sorry.`}</div>
  );
}