// Configuration of API absolute URL
const dev = process.env.NODE_ENV !== 'production';
// configure server url
export const server = dev ? 'http://localhost:5000' : 'https://your_deployment.server.com'; // without trailing slash cause the api path already provide fore slash
export const imgserver = dev ? 'http://localhost:3000/' : 'https://your_deployment.server.com'; // with trailing slash cause we don't write / before static in props