import { ActionFunction, Form, Link, LoaderFunction, Outlet, useActionData, useLoaderData } from "remix";
import { imgserver } from "~/routes/config";
import { NewMhswRoute } from "~/routes/mhsws";
import RandomMhswRoute from "./randomize";
import { db } from "~/utils/db.server";
import { Mhsw } from "@prisma/client";
import { ValidateRandomInput } from "~/config/validation";


type LoaderData = {
  mhswListItems: Array<{ id: string; name: string; nim: string; batch: string }>;
};

type ActionData = {
  randomMhsw: Mhsw;
};

export const loader: LoaderFunction = async () => {
  const data: LoaderData = {
    mhswListItems: await db.mhsw.findMany()
  };
  // console.log(data)
  return data;
};

export let action: ActionFunction = async ({ request }) => {
  const form = await request.formData()
  const selectedn: number | any = form.get("selectedn")
  // console.log(selectedn), validate data inputted
  let nofrandomitems: string = ValidateRandomInput(selectedn);
  console.log(nofrandomitems)
  const count = await db.mhsw.count();
  const randomRowNumber = Math.floor(Math.random() * (count - parseInt(nofrandomitems))); // since we know already the number of students which is 49, we have to make sure that the number returned if we make selection of n siswa is always give back n items and not less than that
  const randomMhsw = await db.mhsw.findMany({
    take: parseInt(nofrandomitems),
    skip: randomRowNumber
  });
  const anotherdata = { randomMhsw };
  // console.log(anotherdata)
  return anotherdata;
}

function randomColor() {
  let colorspace = ["#edbb99", "#fdebd0", "#a9dfbf", "#aed6f1", "#f7dc6f", "#73c6b6"];
  const count = colorspace.length;
  const randomNumber = Math.floor(Math.random() * count);
  let selectedcolor = colorspace[randomNumber]
  return selectedcolor
}

export default function Index() {
  let data = useLoaderData<LoaderData>();
  // console.log(data)
  let datarandom = useActionData()
  console.log(datarandom)
  let container = false;
  if (!datarandom) {
    container = false;
  } else {
    container = true;
  }
  
  return (
    <div className="d-flex flex-column justify-content-center">
      <header className="shadow sticky-top">
        <div className="main-navigation d-flex justify-content-between">
          <Link to="/" title="SimProcu" className="d-flex main-logo">
            <MyShufflerLogo imglink="static/img/remix-shuffler-logo.png" />
          </Link>
          <nav aria-label="Main navigation" className="d-flex flex-row main-nav-container">
            <ul className="d-flex align-items-center">
              <li className="clean-list-items nav-item main-nav-items">
                <a className="text-decoration-none text-muted" href="https://gitlab.com/febrifahmi/remix-shuffler">Febri Fahmi's Gitlab</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
      <div className="d-flex flex-row" style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.4" }}>
        <div className="d-flex flex-column col-4 layout-left-side">
          <NewMhswRoute />
          {/* <GetAllMhsws /> <--- cara seperti ini error, loader datanya harus langsung di parent route */}
          <div className="gap-to-top-3 container justify-content-center">
            <div className="d-flex justify-content-center">
              <h5 className="d-flex">Data Mahasiswa</h5>
            </div>
            <div className="card d-flex flex-row flex-wrap gap-to-top-1 int-atas-bwh justify-content-center container">
              {data.mhswListItems.map(mhsw => (
                <div className="card card-grey d-flex small-font gap-to-left-sm gap-to-top-025 sm-padding" key={mhsw.id}>
                  <Link className="clean-link text-muted" to={mhsw.id}>{mhsw.name}&nbsp;({mhsw.nim})</Link>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="col-8">
          <div className="card">
            <Form method="post">
              <div>
                <label>
                  Masukkan angka:&nbsp;
                </label>
                <input type="text" name="selectedn"></input>
                <button type="submit">
                  Set
                </button>
              </div>
            </Form>
          </div>
          <div className="d-flex align-items-center gap-to-top-2">
            {container ? <div className="container d-flex flex-row flex-wrap justify-content-center">{datarandom.randomMhsw.map((item: any) => <div className="card int-atas-bwh big-card sm-padding gap-to-left-md gap-to-top-1 popOver" style={{ backgroundColor: randomColor() }}><div className="counter">. {item.name}</div></div>)}</div> : <div className="d-flex justify-content-center align-items-center"><h2 className="d-flex">Random Me!</h2></div>}
          </div>
        </div>
      </div>
    </div>
  );
}


function MyShufflerLogo({ imglink }: any) {
  let basepath = imgserver;
  let image = {
    url: basepath + imglink
  }
  // console.log(image)
  return (
    <img src={image.url} width="176px" height="38px"></img>
  )
}


export function CatchBoundary() { }