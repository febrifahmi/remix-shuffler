import { ActionFunction, Form, LoaderFunction, redirect, useTransition } from "remix";
import { Link, useLoaderData } from "remix";
import { db } from "~/utils/db.server";

// type LoaderData = {
//     mhswListItems: Array<{ id: string; name: string; nim: string; batch: string }>;
// };

// export const loader: LoaderFunction = async () => {
//     const data: LoaderData = {
//         mhswListItems: await db.mhsw.findMany()
//     };
//     console.log(data)
//     return data;
// };

export const action: ActionFunction = async ({
    request
}) => {
    const form = await request.formData();
    const name = form.get("name");
    const nim = form.get("nim");
    const batch = form.get("batch");
    // we do this type check to be extra sure and to make TypeScript happy
    // we'll explore validation next!
    if (
        typeof name !== "string" ||
        typeof nim !== "string" ||
        typeof batch !== "string"
    ) {
        throw new Error(`Form not submitted correctly.`);
    }

    const fields = { name, nim, batch };

    const mhsw = await db.mhsw.create({ data: fields });
    return redirect("/");
};

export function NewMhswRoute() {
    const { state } = useTransition();
    const busy = state === "submitting";
    return (
        <div className="col gap-to-top-2 d-flex flex-column justify-content-center">
            <div className="col-9 container">
                <div className="d-flex justify-content-center">
                    <h3 className="d-flex">Tambahkan data mahasiswa</h3>
                </div>
                <Form className="col d-flex gap-to-top-2 flex-column container" method="post" action="/mhsws">
                    <div className="d-flex flex-fill">
                        <label className="col-3">
                            Nama:
                        </label>
                        <input className="col" type="text" name="name" />
                    </div>
                    <div className="d-flex flex-fill gap-to-top-1">
                        <label className="col-3">
                            NIM:
                        </label>
                        <textarea className="col" name="nim" />
                    </div>
                    <div className="d-flex flex-fill gap-to-top-1">
                        <label className="col-3">
                            Batch:
                        </label>
                        <textarea className="col" name="batch" />
                    </div>
                    <div className="d-flex flex-fill gap-to-top-1 justify-content-center">
                        <button type="submit" className="button btn-lg" disabled={busy}>
                        {busy ? "Menambahkan..." : "Tambah"}
                        </button>
                    </div>
                </Form>
            </div>
        </div>
    );
}

// export function GetAllMhsws() {
//     const data = useLoaderData<LoaderData>();
//     console.log(data)
//     return (
//         <div className="gap-to-top-2">
//             {data.mhswListItems.map(mhsw => (
//                 <div className="card" key={mhsw.id}>
//                     <Link to={mhsw.id}>{mhsw.name}&nbsp;{mhsw.nim}&nbsp;{mhsw.batch}</Link>
//                 </div>
//             ))}
//         </div>
//     )
// }

export function CatchBoundary() {}